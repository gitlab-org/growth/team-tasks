# Growth Team Tasks project

This project is primarily an issue tracker for:

* [Team Tasks](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues) for Growth team specific tasks.

Issues for specific projects are usually created under the following groups: 
* [GitLab.org](https://gitlab.com/groups/gitlab-org/-/issues) for issues relating to projects in gitlab-org (including [GitLab](https://gitlab.com/gitlab-org/gitlab/))
* [GitLab Services](https://gitlab.com/groups/gitlab-org/gitlab-services/-/issues) for issues relating to projects in gitlab-services (including [Versions](https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com))
* [GitLab.com](https://gitlab.com/groups/gitlab-com/-/issues) for issues relating to projects in gitlab-com projects (including [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/))
