<!-- title: {Team Member} OOO from Jun 18 - Jun 28 -->

## :o: Opening Tasks

- [ ] Assign to yourself
- [ ] Assign due date for your last PTO day
- [ ] Fill in the Ongoing work table with broad based responsibilities, re-assigning work as needed. 
- [ ] Assign to anyone with a specific task or responsibility assigned to them.
- [ ] Merge request coverage
   - [ ] Time sensitive: Assign another teammate, in case it gets merged and makes it to production.
   - [ ] Not Time sensitive: Set to `Draft:` status so that it does not merge while on PTO.
- [ ] Issue coverage for `~"workflow::in dev"` or later in the workflow.
   - [ ] Time sensitive: Assign another teammate, to continue work and guide to completion. Include an updated status/handoff comment.
   - [ ] Not Time sensitive: Include an updated status comment.
- [ ] Time Off by Deel, using this issue as a link in the description and assign the responsibility to the `#g_acquisition` slack channel at least 2-3 business days before the scheduled PTO begins.
- [ ] Not on-call during PTO
- [ ] Add an issue comment for your :recycle: Retrospective Thread
- [ ] Updated Google Calendar by [setting yourself out of office](https://support.google.com/calendar/answer/7638168#:~:text=of%20a%20day-,Show%20when%20you%E2%80%99re%20out%20of%20office,-When%20you%20indicate) and confirm events are auto-declined.

## :palm_tree: Last day before going on PTO Tasks

- [ ] Updated GitLab Profile to :palm_tree:

## :construction: Ongoing work

<!-- Optional: Ideal scenario is that there isn't much/anything to hand off, but sometimes that is unavoidable

Including prioritizing issues, responding to clarification in current in-progress work, etc.

| Theme | Context Link | Primary |
| ----- | ------------ | ------- |
|       |              |         |
|       |              |         |

--> 

## :x: Closing Tasks

- [ ] Review the [Returning from Time Off ](https://about.gitlab.com/handbook/paid-time-off/#returning-from-pto) guidance
- [ ] **PM only**: Review the [PM Returning from Time Off ](https://about.gitlab.com/handbook/product/product-manager-role/#returning-from-time-off) guidance
- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-org/enablement-section/discussions/-/blob/main/.gitlab/issue_templates/pto_coverage.md)

## :notebook: Additional Notes

<!-- Optional: if you prefer, you can make this issue confidential to members of `gitlab-org` using the quick action: --> 
<!-- /confidential -->

<!-- https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off -->

/assign me
