# Growth Fullstack Engineering - additional onboarding

The following onboarding tasks are in addition to the standard developer onboarding (including backend and frontend) 
and are specific to fullstack engineers in the Growth sub-department.

## Growth Group/Tool Memberships

- [ ] Ask your manager to add you as a developer in `@gitlab-org/growth` and relevant sub-groups (experiment-devs, your team group)

## Technical Writing

- [ ] Take the [GitLab Technical Writing Fundamentals](https://handbook.gitlab.com/handbook/product/ux/technical-writing/fundamentals/)
      course. This will take approximately 2 hours to complete.

## Snowplow Onboarding Template

The following tasks come from https://gitlab.com/gitlab-org/analytics-section/analytics-instrumentation/internal/-/blob/master/.gitlab/issue_templates/snowplow_onboarding.md

### Send and receive Snowplow Events from GitLab to Snowplow Micro

The goal is to introduce you to how Snowplow works. Your first task is to locally replicate the sending and receiving
of Snowplow events. In order to do this, you will traverse the gitlab and snowplow codebases to see how a snowplow
event is sent and collected.

#### Steps

Please refer to the suggested reading below at any time for more context on any of the following steps.

- [ ] Read the [Analytics Instrumentation Guide](https://handbook.gitlab.com/handbook/product/analytics-instrumentation-guide/)
- [ ] Read the [Snowplow Guide](https://archives.docs.gitlab.com/16.4/ee/development/internal_analytics/snowplow/index.html), the page was removed in GitLab 16.5, still useful in some cases.
- [ ] Set-Up GitLab Development Environment https://gitlab.com/gitlab-org/gitlab-development-kit. The [one-line installation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation) will automatically clone & set-up the GitLab repo inside the `gitlab-development-kit/gitlab` folder. If you experience any issues, reach out to the team or search in the Slack channel `#gdk`. 
  - [ ] Read [Guidelines for implementing Enterprise Edition features](https://docs.gitlab.com/ee/development/ee_features.html) - Ensuring GDK is in Act as SaaS mode when needed.
  - [ ] After setting up GDK, [browse your local GitLab development server](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/browse.md)
  - [ ] [Start the Rails console](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/rails_console.md)
  - [ ] [Install and Run Snowplow Micro](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/snowplow_micro.md)
    - [ ] Open an access request to be added to our(GitLab) Docker Hub organization to enable use of Docker Desktop. 
          See [this example request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/14795)
- [ ] Add a Snowplow event using HAML https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/migration.html#frontend
- [ ] Add a Snowplow event using Ruby https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/migration.html#backend
- [ ] Using your browser, navigate to wherever the event was added and trigger all the added Snowplow events (HAML, Ruby)
- [ ] In Snowplow Micro, ensure all of the above mentioned events are successfully captured as good events in `localhost:9091/micro/good`

#### Request GitLab Team Member License

- [ ] Start a [GitLab Team Member License request](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/policies/team_member_licenses/)
- [ ] Add the license you receive to your GDK instance using [this guide](https://docs.gitlab.com/ee/user/admin_area/license.html#activate-gitlab-ee-with-a-license-file).

## CustomersDot setup (if applicable to your group)

- [ ] Complete [setup](https://gitlab.com/gitlab-org/customers-gitlab-com#setup) of CustomersDot development environment

## Getting familiar with Experiments at GitLab

- [ ] Pair and implement an experiment using [GLEX](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment)
   - Your manager will provide you with an issue and your pairing partner should be a teammate familiar with running experiments.
- [ ] Implement an experiment on your own 
- [ ] Consider becoming a reviewer for [GLEX](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment) 
  - Open a merge request to add a [project role](https://gitlab.com/gitlab-com/www-gitlab-com/blob/cd40ba978ec3ac078b05e46bc1408ae9d14ce371/data/team_members/person/README.md#L74)
    and assign to your manager

## Get to know UX

- [ ] Set up a coffee chat with the [product designers who work with your team](https://handbook.gitlab.com/handbook/product/categories/#growth-section)
- [ ] Familiarize yourself with the process of [UX reviews of MRs](https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines). 
- [ ] Familiarize yourself with [documenting your changes](https://docs.gitlab.com/ee/development/contributing/design.html#merge-request-reviews) in the MR for review. You can also use videos instead of screenshots for interactive functionality.
- [ ] Familiarize yourself with our [design system](https://design.gitlab.com/)
- [ ] Familiarize yourself with our [components library](https://gitlab-org.gitlab.io/gitlab-ui/)

## Suggested reading/video materials

Note that it may be helpful to save the following to reference at a later time as well.

- [ ] [Growth Sub-department](https://handbook.gitlab.com/handbook/marketing/growth/engineering/)
  - [ ] [Experimentation in Growth](https://about.gitlab.com/handbook/engineering/development/growth/#experimentation-1)
  - [ ] [Read about the experimentation process](https://handbook.gitlab.com/handbook/marketing/growth/engineering/experimentation/)
  - [ ] [Experiment Guide developer documentation](https://docs.gitlab.com/ee/development/experiment_guide/)
  - [ ] [GLEX README](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/)
  - [ ] [GLEX implementation examples knowledge base](https://gitlab.com/groups/gitlab-org/growth/-/wikis/GLEX-How-Tos)
  - [ ] [Experiments API documentation](https://docs.gitlab.com/ee/api/experiments.html) (see also https://gitlab.com/api/v4/experiments)
- [ ] [Growth direction](https://handbook.gitlab.com/handbook/marketing/growth/)
- [ ] [Growth product handbook](https://about.gitlab.com/handbook/product/growth/)
- [ ] [Growth playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf) (review list, watch any relevant recordings)
  - [ ] Specifically make sure to watch [How to launch product experiments at GitLab](https://www.youtube.com/watch?v=rEHxAfxr9eU)
