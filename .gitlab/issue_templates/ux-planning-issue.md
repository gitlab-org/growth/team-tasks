[Growth PTO doc](https://docs.google.com/document/d/1GfQ0NFCZdD8WBOCqFG1GWRH8NWH_ZGDVIY7oB5iY4Fo/edit)
[Growth UX Board](https://gitlab.com/groups/gitlab-org/-/boards/1254597?scope=all&label_name[]=UX&label_name[]=devops%3A%3Agrowth&milestone_title=14.6)

## Kevin

* [Adoption planning issue]()
* [Conversion planning issue]()

Other UX work:
* ...

## Emily

* [Activation planning issue]()
* [Expansion planning issue]()

Other UX work:
* ...

## Nick H.
* ...

## Matej

* ...

## Checklist

Check your name on the list when you reviewed and added your priorities for this milestone to the issue description here.

- [ ] @kcomoli 
- [ ] @emilybauman 
- [ ] @NickHertz 
- [ ] @matejlatin

/label ~UX ~'devops::growth' ~'section::growth' ~'Planning Issue'
