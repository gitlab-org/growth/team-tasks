The following is the ~"group::acquisition" planning template for MILESTONE

### Process and timeline
<details>
  <summary>Click to expand!</summary>

  #### Process
  * https://about.gitlab.com/handbook/product-development-flow/
  * https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline
  * https://about.gitlab.com/handbook/engineering/development/growth/
  * https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/growth/#how-we-work
  * Please add/update/change any information directly in the description. Add a comment if you think the change needs an explanation.
  * We link to but won't discuss details of security issues here, as this issue is public
</details>

### Notice
:warning: We're transitioning from embedding issue lists in description of planning issue to relying on boards for managing the planned work.

### Capacity

All: update the doc to indicate time off / capacity for the upcoming milestone

* [Growth sub-department milestone capacity log](https://docs.google.com/document/d/1GfQ0NFCZdD8WBOCqFG1GWRH8NWH_ZGDVIY7oB5iY4Fo/edit)
* [Engineering Capacity Spreadsheet](https://docs.google.com/spreadsheets/d/1YY5Xvv_5IaqvVmpvlZZQukg_MVuD7p0q0nbYYmejylo/edit?usp=sharing)

### Planning

The following boards can be further filtered by milestone. Please check off after review:

- [ ] [Growth groups](https://gitlab.com/groups/gitlab-org/-/boards/2185131?label_name[]=devops%3A%3Agrowth) - are there issues that need to be moved to another Growth group?
- [ ] [By work classification](https://gitlab.com/groups/gitlab-org/-/boards/4459085) - are there issues in the `Open` column that need to be classified?
- [ ] [Bugs by severity](https://gitlab.com/groups/gitlab-org/-/boards/1508460) - are there issues that need to be included in the next milestone?
- [ ] [SUS-impacting](https://gitlab.com/groups/gitlab-org/-/boards/3847948?label_name[]=SUS%3A%3AImpacting&label_name[]=group%3A%3Aacquisition) - should any ~"SUS::Impacting" issues be moved into this milestone?
- [ ] [Full workflow](https://gitlab.com/groups/gitlab-org/-/boards/1778664?label_name[]=group%3A%3Aacquisition&milestone_title=Any)
  - [Validation workflow](https://gitlab.com/groups/gitlab-org/-/boards/1506660?label_name[]=group%3A%3Aacquisition) (PM/UX)
  - [Build workflow](https://gitlab.com/groups/gitlab-org/-/boards/1506701?milestone_title=Any%20Milestone&label_name[]=group%3A%3Aacquisition) (Development)
  - [Assigned engineer](https://gitlab.com/groups/gitlab-org/-/boards/1317251?label_name[]=group%3A%3Aacquisition) (Development)

Filter all planned issues by the current milestone and ensure ordering by column is correct:
- [ ] [All planned issues](https://gitlab.com/groups/gitlab-org/-/boards/4152983)

### Cross-functional dashboard review

https://about.gitlab.com/handbook/product-development-flow/cross-functional-dashboard-reviews.html

- [Growth](https://about.gitlab.com/handbook/engineering/development/growth/#cross-functional-backlog)
  - [Acquisition](https://about.gitlab.com/handbook/engineering/development/growth/acquisition/#cross-functional-backlog)
  - [Activation](https://about.gitlab.com/handbook/engineering/development/growth/activation/#cross-functional-backlog)

Undefined merge requests

- [Open](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&not[label_name][]=type%3A%3Amaintenance&not[label_name][]=type%3A%3Abug&not[label_name][]=type%3A%3Afeature&label_name[]=group%3A%3Aacquisition)
- [Merged](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=merged&not[label_name][]=type%3A%3Amaintenance&not[label_name][]=type%3A%3Abug&not[label_name][]=type%3A%3Afeature&label_name[]=group%3A%3Aacquisition)

Reviewed by:
- [ ] PM @gdoud
- [ ] EM @kniechajewicz
- [ ] UX @kcomoli / cc @jackib
- [ ] Quality  @vincywilson

/label ~"section::growth" ~"devops::growth" ~"group::acquisition" ~"Planning Issue" ~"type::maintenance"
