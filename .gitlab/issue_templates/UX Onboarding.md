Welcome to the Growth team! Let's use this issue to list specific resources for you to read and get familiar with. You can ask any questions here too. This issue is meant for you to work on AFTER your other onboarding issues :)


## Introduce Yourself
- [ ] Add a slide about yourself to the [team slide deck](https://docs.google.com/presentation/d/17VQTqtYumlaFBLyxGvdq0pUspibU_mB2X0G_fz9eVgU/edit?usp=sharing)

## Handbook pages
- [Growth UX Team page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/growth/)
- [Growth Sub-department](https://about.gitlab.com/handbook/engineering/development/growth/)
- [How the Growth section works](https://about.gitlab.com/handbook/product/growth/)


## Other reading and watching
- Blog post - https://about.gitlab.com/blog/2020/02/27/how-holistic-ux-design-increased-gitlab-free-trial-signups/
- Article — https://www.mckinsey.com/business-functions/mckinsey-design/our-insights/the-business-value-of-design
- [Hacking Growth](https://www.amazon.com/Hacking-Growth-Fastest-Growing-Companies-Breakout/dp/045149721X) You can expense this book if you don't already have it.
- [UX Showcases](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz) are presentations product designer give on work and topics they find interesting.
     - [Sample of recent experiments](https://www.youtube.com/watch?v=uV0alFo_wfI&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=4)
     - [New User Onboarding](https://www.youtube.com/watch?v=NLDzr69D2Pw&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=11)
     - [Getting started as a designer](https://www.youtube.com/watch?v=wWPD5EPyou4&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=37)
- Growth Related UX Scorecards - https://gitlab.com/groups/gitlab-org/-/epics/2015
- Growth related user flow - [Figma](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=0%3A1)

## Our focus areas right now
- A big area of focus is 

## Getting to know the product
There will be some training on this during your regular onboarding issues. Once you are done with those, we suggest a couple areas for you to look at more carefully, as they are related to the work you'll be doing. You can create a personal project for this, if you'd like. 
- [ ] Create an account, start a trial
- [ ] Create a group or groups
- [ ] Create a project and repo
- [ ] Invite someone to your project. It can be me, or your buddy, or anyone you like.
- [ ] Create a merge request, "review it", and merge it.
- [ ] Set up and run a CI/CD pipeline

**It would be awesome if you took some notes on this process as you go through it, because we're always thinking about new user experiences, and we know improvements are needed. So feel free to capture your thoughts in an issue or google doc and share them with the team later.**
